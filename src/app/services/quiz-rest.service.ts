import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Quiz } from '../store/quizzes/quizzes.model';
@Injectable({
  providedIn: 'root'
})
export class QuizRestService {
  url = "http://localhost:8080/";
  httpHeader = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(private http: HttpClient) { }
  /**
   * POST Quiz
   * @param quiz
   * @returns
   */
  saveQuiz(quiz: Quiz): Observable<any> {

    return this.http.post<any>(this.url+"quiz", quiz, this.httpHeader)
      .pipe(
        catchError(this.handleError<Quiz>('Add Quiz'))
      );
  }
  /**
   * GET Quizzes
   * @returns
   */
  getQuizList(): Observable<Quiz[]> {
    console.log(this.httpHeader);
    return this.http.get<Quiz[]>(this.url+"quizzes")
      .pipe(
        catchError(this.handleError<Quiz[]>('Get Quizzes', []))
      );
  }
  /**
   *
   * @param operation
   * @param result
   * @returns
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}
