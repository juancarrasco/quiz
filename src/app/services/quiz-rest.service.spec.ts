import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { QuizRestService } from './quiz-rest.service';

describe('QuizRestService', () => {
  let service: QuizRestService;
  let httpclientSpy: { post: jasmine.Spy, get: jasmine.Spy }
  beforeEach(() => {

    httpclientSpy = jasmine.createSpyObj('HttpClient', ["post", "get"])

    service = new QuizRestService(httpclientSpy as any);
  });

  it('should be created', () => {
    let body = {
      "email": "juanandrescarrasco91@gmail.com",
      "gender": "POP"
    }
    let responseService = {
      "id": 1,
      "email": "juanandrescarrasco91@gmail.com",
      "gender": "POP"
    }
    httpclientSpy.post.and.returnValue(of(responseService))
    service.saveQuiz(body).subscribe(resultado => {
      expect(resultado).toEqual(responseService)
    })
  });

  it('should be get', () => {
    let responseService = [{
      "id": 1,
      "email": "juanandrescarrasco91@gmail.com",
      "gender": "POP"
    }]
    httpclientSpy.get.and.returnValue(of(responseService))
    service.getQuizList().subscribe(resultado => {
      expect(resultado).toEqual(responseService)
    })
  });
});
