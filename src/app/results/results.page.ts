import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { QuizzesState } from '../store/quizzes/quizes.state';
import { LoadQuizzes } from '../store/quizzes/quizzes.actions';
import { Quiz } from '../store/quizzes/quizzes.model';
import LinearGradient from 'zrender/lib/graphic/LinearGradient';
import { EChartsOption } from 'echarts';

@Component({
  selector: 'app-results',
  templateUrl: './results.page.html',
  styleUrls: ['./results.page.scss'],
})
export class ResultsPage implements OnInit, AfterViewInit, OnDestroy   {
  @Select(QuizzesState.getQuizzes) quizzes: Observable<Quiz[]>;
  subscribe:Subscription;

  options: EChartsOption ;
  initValues = [
    0,0,0,0,0
  ];
  data = [...this.initValues];
  dataAxis = [
    "ROCK", "POP", "JAZZ", "CLASICA", "ETC"
  ];
  dataShadow= [];



  constructor(
    private store: Store
  ) {
    this.setOptions()
    this.store.dispatch(new LoadQuizzes())
  }
  ngOnInit(): void {
    this.subscribe = this.quizzes.subscribe(data =>{
      this.data = [...this.initValues]

      data.forEach(element => {
        const index = this.dataAxis.indexOf(element.gender)
        this.data[index]++
      });
      if(data.length > 0)
      this.setDataChart();
    })




  }

  ngAfterViewInit() {
    this.setDataChart()
  }

  ngOnDestroy() {
    this.subscribe.unsubscribe();
  }

  setOptions(){
    console.log(this.data)
    const yMax = 10;
    this.dataShadow = [];

    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < this.data.length; i++) {
      this.dataShadow.push(yMax);
    }

    this.options = {
      title: {
        text: 'Encuestas 3it',
      },
      grid: { left: '25px', bottom: '20px', height: 'auto' },
      xAxis: {
        data: this.dataAxis,
        axisLabel: {
          inside: true,
          color: '#000000',
        },
        axisTick: {
          show: false,
        },
        axisLine: {
          show: false,
        },
        z: 15,
      },
      yAxis: {
        axisLine: {
          show: false,
        },
        axisTick: {
          show: false,
        },
        axisLabel: {

        },
      },
      dataZoom: [
        {
          type: 'inside',
        },
      ],
      series: [
        {
          // For shadow
          type: 'bar',
          itemStyle: {
            color: 'rgba(0,0,0,0.05)'
          },
          barGap: '-100%',
          barCategoryGap: '40%',
          data: this.dataShadow,
          animation: false,
        },
        {
          type: 'bar',
          itemStyle: {
            color: new LinearGradient(0, 0, 0, 1, [
              { offset: 0, color: '#83bff6' },
              { offset: 0.5, color: '#188df0' },
              { offset: 1, color: '#188df0' },
            ]),
          },
          emphasis: {
            itemStyle: {
              color: new LinearGradient(0, 0, 0, 1, [
                { offset: 0, color: '#2378f7' },
                { offset: 0.7, color: '#2378f7' },
                { offset: 1, color: '#83bff6' },
              ]),
            }
          },
          data:[...this.data],
        },
      ],
    };
  }

  setDataChart() {
    this.options.series = [
      {
        // For shadow
        type: 'bar',
        itemStyle: {
          color: 'rgba(0,0,0,0.05)'
        },
        barGap: '-100%',
        barCategoryGap: '40%',
        data: this.dataShadow,
        animation: false,
      },
      {
        type: 'bar',
        itemStyle: {
          color: new LinearGradient(0, 0, 0, 1, [
            { offset: 0, color: '#83bff6' },
            { offset: 0.5, color: '#188df0' },
            { offset: 1, color: '#188df0' },
          ]),
        },
        emphasis: {
          itemStyle: {
            color: new LinearGradient(0, 0, 0, 1, [
              { offset: 0, color: '#2378f7' },
              { offset: 0.7, color: '#2378f7' },
              { offset: 1, color: '#83bff6' },
            ]),
          }
        },
        data:[...this.data],
      },
    ]
  }
}
