import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { IonicModule } from '@ionic/angular';
import { NgxsModule } from '@ngxs/store';
import { QuizRestService } from '../services/quiz-rest.service';
import { QuizzesState } from '../store/quizzes/quizes.state';

import { ResultsPage } from './results.page';

describe('ResultsPage', () => {
  let component: ResultsPage;
  let fixture: ComponentFixture<ResultsPage>;
  class ApiServiceMock{
    saveQuiz(){
      return ''
    }
    getQuizList(){
      return ''
    }
  }
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultsPage ],
      imports: [
        IonicModule.forRoot(),
        NgxsModule.forRoot([QuizzesState]),
        RouterTestingModule,
        HttpClientModule,
      ],
      providers: [{
        provide: QuizRestService,
        useClass: ApiServiceMock,
      }]
    }).compileComponents();

    fixture = TestBed.createComponent(ResultsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
