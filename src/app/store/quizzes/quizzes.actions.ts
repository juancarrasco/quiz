import { Quiz } from './quizzes.model';

export class LoadQuizzes {
  static readonly type = '[Quiz] Load';
  constructor() {}
}
export class saveQuiz {
  static readonly type = '[Quiz] add';
  constructor( public payload: Quiz ) {}
}
