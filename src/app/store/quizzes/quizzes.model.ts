export class QuizzesStateModel {
  quizzes: Quiz[];
  error?:any;
}
export interface Quiz {
  id?: number;
  email: string;
  gender: string;
}
