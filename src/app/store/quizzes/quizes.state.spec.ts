import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { NgxsModule, Store } from '@ngxs/store';
import { of } from 'rxjs';
import { QuizRestService } from 'src/app/services/quiz-rest.service';
import { QuizzesState } from './quizes.state';
import { LoadQuizzes } from './quizzes.actions';

export const SOME_DESIRED_STATE = {
  quizzes: []
};
class ApiServiceMock {
  saveQuiz() {
    return ''
  }
  getQuizList() {

    return ""
  }
}
describe('Quizzes', () => {
  let store: Store;
  const httpClientSpy = jasmine.createSpyObj('HttpClient', ['post', 'get']);

  beforeEach(() => {
    httpClientSpy.get.and.returnValue(of([{
      "id": 1,
      "email": "juanandrescarrasco91@gmail.com",
      "gender": "POP"
    }]));

    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([QuizzesState]), HttpClientModule],
      providers: [{
        provide: QuizRestService,
        useClass: ApiServiceMock,
      }]
    });

    store = TestBed.inject(Store);
    store.reset({
      ...store.snapshot(),
      quizzes: SOME_DESIRED_STATE
    });
  });

  it('Call load quizzes', () => {
    store.dispatch(new LoadQuizzes());
    const quizzesState = store.selectSnapshot(state => state.quizzes);
    expect(quizzesState.quizzes).toEqual([]);
  });
});
