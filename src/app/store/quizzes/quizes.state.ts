import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Quiz, QuizzesStateModel } from './quizzes.model';
import { LoadQuizzes, saveQuiz } from './quizzes.actions';
import { QuizRestService } from 'src/app/services/quiz-rest.service';
import { Injectable } from '@angular/core';

@State<QuizzesStateModel>({
  name: 'quizzes',
  defaults: {
    quizzes: []
  }
})
@Injectable()
export class QuizzesState {
  constructor(public quizRestService: QuizRestService) { }
/**
 * get array of quiz
 * @param state QuizzesStateModel
 * @returns
 */
  @Selector()
  static getQuizzes(state: QuizzesStateModel) { return state.quizzes; }

  /**
   * get error in call http
   * @param state QuizzesStateModel
   * @returns
   */
  @Selector()
  static getQuizzesError(state: QuizzesStateModel) { return state.error }

  /**
   * load quizzes entity
   * @param StateContext
   * @returns
   */
  @Action(LoadQuizzes)
  load({ getState, setState }: StateContext<QuizzesStateModel>) {
    const state = getState();
    console.log("LoadQuizzes")
    return this.quizRestService.getQuizList().subscribe((data: Quiz[]) => {
      setState({
        quizzes: data
      });
    });
  }

  // Añade un nuevo post al estado
  @Action(saveQuiz)
  save({ getState, setState, patchState }: StateContext<QuizzesStateModel>, { payload }: saveQuiz) {
    const state = getState();
    console.log("saveQuiz", payload)
    return this.quizRestService.saveQuiz(payload).subscribe((data: String) => {
      let error = (data)?false:true;
        setState({
          ...state,
          error
        });
    });
  }

}
