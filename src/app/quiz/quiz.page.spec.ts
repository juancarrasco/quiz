import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { IonicModule } from '@ionic/angular';
import { NgxsModule } from '@ngxs/store';
import { QuizRestService } from '../services/quiz-rest.service';
import { QuizzesState } from '../store/quizzes/quizes.state';

import { QuizPage } from './quiz.page';
class ApiServiceMock{
  saveQuiz(){
    return ''
  }
  getQuizList(){
    return ''
  }
}
describe('QuizPage', () => {
  let component: QuizPage;
  let fixture: ComponentFixture<QuizPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ QuizPage ],
      imports: [
          IonicModule.forRoot(),
          NgxsModule.forRoot([QuizzesState]),
          RouterTestingModule,
          HttpClientModule,
        ],
        providers: [{
          provide: QuizRestService,
          useClass: ApiServiceMock,
        }]
    }).compileComponents();

    fixture = TestBed.createComponent(QuizPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
