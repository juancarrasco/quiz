import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { saveQuiz } from '../store/quizzes/quizzes.actions';
import { AlertController } from '@ionic/angular';
import { QuizzesState } from '../store/quizzes/quizes.state';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.page.html',
  styleUrls: ['./quiz.page.scss'],
})
export class QuizPage implements OnInit, OnDestroy {
  myForm: FormGroup;
  genders = [
    "ROCK", "POP", "JAZZ", "CLASICA", "ETC"
  ];
  constructor(
    public alertController: AlertController,
    private store: Store,
    public _router: Router
  ) { }
  ngOnDestroy(): void {
    this.subscribe.unsubscribe()
  }
  getQuizzesError
  @Select(QuizzesState.getQuizzesError) error: Observable<any>;
  subscribe:Subscription;

  async ngOnInit() {
    this.myForm = new FormGroup({
      email: new FormControl('',
        [
          Validators.email,
          Validators.required,
          Validators.minLength(4)
        ]),
      gender: new FormControl('',
        [
          Validators.required,
        ])
    });
    this.subscribe = this.error.subscribe(async data => {
      if (data) {
        const alert = await this.alertController.create({
          cssClass: 'my-custom-class',
          header: 'Datos no aceptados',
          subHeader: 'Ohh parece que ya estas registrado !',
          message: 'Intenta con un email diferente',
        });
        await alert.present();
        return;
      } else if(data === false) {
        const alert = await this.alertController.create({
          cssClass: 'my-custom-class',
          header: 'Datos Guardados',
          subHeader: 'Datos ingresados correctamente!',
          message: 'Revisa tus resultados.',
          buttons: ['OK']
        });
        await alert.present();
        await alert.onDidDismiss();
        this._router.navigateByUrl('/results');
        return;
      }
    })
  }
  async onSubmit(form: FormGroup) {
    console.log('Valid?', form.valid); // true or false
    console.log('Name', form.value.email);
    if (!form.valid) {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Formulario mal formado',
        subHeader: 'Revisa los datos ingresados!',
        message: 'Email o Genero',
        buttons: ['OK']
      });
      await alert.present();
      return;
    }
    let { email, gender } = form.value;
    console.log('Email', form.value.gender);
    this.store.dispatch(new saveQuiz({ email, gender }))
  }
}
