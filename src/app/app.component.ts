import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Resultados', url: '/results', icon: 'clipboard' },
    { title: 'Encuesta', url: '/quiz', icon: 'brush' },
    { title: 'Acerca De', url: '/about', icon: 'heart' },
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor() {}
}
